package terminal;

import java.nio.file.*;
import java.util.Scanner;

public class Terminal {

    public static void main(String[] args) {

        boolean salida = true;
        java.nio.file.Path ruta = Paths.get(System.getProperty("user.dir"));

        while (salida) {

            String opcion = "";

            System.out.println(ruta + ">");
            Scanner teclado = new Scanner(System.in);
            opcion = teclado.nextLine();
            String[] arr;
            arr = opcion.split(" ");

            switch (arr[0]) {
                case "help":

                    help();
                    break;
                case "cd":

                    ruta = cd(arr, ruta);
                    break;
                case "close":  //esto te lo he copiado 

                    salida = false;
                    break;
                case "clear":

                    clear();
                    break;
                default:

                    System.out.println("No vayas de inteligente y escribe help para ver los comandos diponibles, gracias.");
                    break;
            }
        }

    }

    public static void help() {

        System.out.println("help -> Lista los comandos con una breve definición de lo que hacen.\n"
                + "cd-> Muestra el directorio actual.\n"
                + "  [..] -> Accede al directorio padre.\n"
                + "  [<nombreDirectorio>] -> Accede a un directorio dentro del directorio actual\n"
                + "  [<rutaAbsoluta] -> Accede a la ruta absoluta del sistema.\n"
                + "mkdir <nombre_directorio> -> Crea un directorio en la ruta actual."
                + "info <nombre> -> Muestra la información del elemento. Indicando FileSystem, Parent, Root, Nº of elements, FreeSpace, TotalSpace y UsableSpace\n"
                + "cat <nombreFichero> -> Muestra el contenido de un fichero.\n"
                + "top <numeroLineas> <nombreFichero> -> Muestra las líneas especificadas de un fichero.\n"
                + "mkfile <nombreFichero> <texto> -> Crea un fichero con ese nombre y el contenido de texto.\n"
                + "write <nombreFichero> <texto> -> Añade 'texto' al final del fichero especificado.\n"
                + "dir -> Lista los archivos o directorios de la ruta actual.\n"
                + " [<nombreDirectorio>] -> Lista los archivos o directorios dentro de ese directorio.\n"
                + "  [<rutaAbsoluta] -> Lista los archivos o directorios dentro de esa ruta.\n"
                + "delete <nombre> -> Borra el fichero, si es un directorio borra todo su contenido y a si mismo.\n"
                + "close -> Cierra el programa.\n"
                + "clear -> vacía la vista."
        );

    }

    public static void clear() {

        System.out.println(" \n"
                + " \n"
                + " \n"
                + " \n"
                + " \n"
                + " \n"
                + " \n"
                + " \n"
                + " \n"
                + " \n"
                + " \n"
                + " \n"
                + " \n"
                + " \n"
                + " \n"
                + " \n"
                + " \n"
                + " \n"
                + " \n"
                + " \n"
                + " \n"
                + " \n"
                + " \n"
                + " \n"
                + " \n"
                + " \n"
                + " \n"
                + " \n"
                + " \n"
                + " \n"
                + " \n"
                + " \n"
                + " \n"
                + " \n"
                + " \n"
                + " \n"
        );

    }

    public static Path cd(String[] arr, Path ruta) {

        if (arr.length > 1) {

            if (arr[1].contains("..")) {

                System.out.println(ruta.getParent());
                ruta = ruta.getParent();

            } else {

                return ruta;

            }

            return ruta;

        }

        return ruta;

    }

}
