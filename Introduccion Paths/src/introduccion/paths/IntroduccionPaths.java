package introduccion.paths;

import java.nio.file.Paths;

public class IntroduccionPaths {
    public static void main(String[] args) {
     
        java.nio.file.Path ruta = Paths.get(System.getProperty("user.dir")); //se obtiene la ruta acutal
        java.nio.file.Path ruta2 = Paths.get("C:\\");  //se puede escribir una ruta en concreto
        
        System.out.println(ruta);
        System.out.println(ruta2);
        
        System.out.println(ruta.resolve("src")); //se añade un nuevo "nodo" al directorio
        
        System.out.println(ruta.getParent()); //se obtiene el padre de la carpeta acutal
        System.out.println(ruta.getName(3)); //segun el int recibido devuelve el nombre de dicha carpeta el cual corresponde a la posicion en el directorio
        System.out.println(ruta.getRoot()); //se obtiene la raiz del directorio
        
    }
    
    public static boolean cd (String[] arr) {
        
        
        java.nio.file.Path ruta = Paths.get(System.getProperty("user.dir"));
        boolean salida = true;
        //ver cuyantos parámetros ha insertado
        
        if (arr[1]=="..") {
            
            System.out.println(ruta.getRoot());
         
        } else {
            
            return salida;
            
        }
        return false;
       
        
        
    }
    
}
